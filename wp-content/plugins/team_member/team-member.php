<?php
/*
Plugin Name: Team Member
Description: Crowd Favorite test
Author: Andrei Ovidiu
Author URI: http://pxlworld.ro
Version: 0.0.1
Text Domain: crowd-fav
License: GPL
*/

defined( 'ABSPATH' ) or die( 'gtfo' );


/**
 * Flushing rewrite rules on plugin activation
 */
function cf_rewrite_flush()
{
	cf_team_member_registration();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'cf_rewrite_flush' );

/**
 * Register Team Member custom post type.
 */
add_action( 'init', 'cf_team_member_registration' );

function cf_team_member_registration() 
{
	$labels = array(
		'name'               => _x( 'Team Members', 'post type general name', 'crowd-fav' ),
		'singular_name'      => _x( 'Team Member', 'post type singular name', 'crowd-fav' ),
		'menu_name'          => _x( 'Team Members', 'admin menu', 'crowd-fav' ),
		'name_admin_bar'     => _x( 'Team Member', 'add new on admin bar', 'crowd-fav' ),
		'add_new'            => _x( 'Add New', 'team member', 'crowd-fav' ),
		'add_new_item'       => __( 'Add New Team Member', 'crowd-fav' ),
		'new_item'           => __( 'New Team Member', 'crowd-fav' ),
		'edit_item'          => __( 'Edit Team Member', 'crowd-fav' ),
		'view_item'          => __( 'View Team Member', 'crowd-fav' ),
		'all_items'          => __( 'All Team Members', 'crowd-fav' ),
		'search_items'       => __( 'Search Team Members', 'crowd-fav' ),
		'parent_item_colon'  => __( 'Parent Team Members:', 'crowd-fav' ),
		'not_found'          => __( 'No team members found.', 'crowd-fav' ),
		'not_found_in_trash' => __( 'No team members found in Trash.', 'crowd-fav' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'crowd-fav' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'team-member' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail')
	);

	register_post_type( 'cf_team_member', $args );
}

/**
 * Register "Department" custom taxonomy
 */
add_action( 'init', 'cf_team_member_taxonomy', 0 );

function cf_team_member_taxonomy()
{
	$labels = array(
		'name'              => _x( 'Departments', 'taxonomy general name', 'crowd-fav' ),
		'singular_name'     => _x( 'Department', 'taxonomy singular name', 'crowd-fav' ),
		'search_items'      => __( 'Search Departments', 'crowd-fav' ),
		'all_items'         => __( 'All Departments', 'crowd-fav' ),
		'parent_item'       => __( 'Parent Department', 'crowd-fav' ),
		'parent_item_colon' => __( 'Parent Department:', 'crowd-fav' ),
		'edit_item'         => __( 'Edit Department', 'crowd-fav' ),
		'update_item'       => __( 'Update Department', 'crowd-fav' ),
		'add_new_item'      => __( 'Add New Department', 'crowd-fav' ),
		'new_item_name'     => __( 'New Department Name', 'crowd-fav' ),
		'menu_name'         => __( 'Department', 'crowd-fav' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'department' ),
	);

	register_taxonomy( 'department', array( 'cf_team_member' ), $args );
}

/**
 * Register Team Member custom meta fields
 */
function cf_team_member_meta_boxes()
{
	add_meta_box( 
    	'cf_member_details', 
    	__( 'Team Member Details', 'crowd-fav' ), 
    	'cf_member_details_callback', 
    	'cf_team_member',
		'normal',
		'high'
    );
}
add_action( 'add_meta_boxes', 'cf_team_member_meta_boxes' );

/**
 * Team Member custom meta fields html
 */
function cf_member_details_callback( $post )
{
	// Add an nonce field so we can check for it later.
	wp_nonce_field( 'cf_details', 'cf_details_nonce' );

    // Use get_post_meta to retrieve an existing value from the database.  
	$cf_position 	= get_post_meta( $post->ID, 'cf_position', true );
	$cf_twitter 	= get_post_meta( $post->ID, 'cf_twitter', true );
	$cf_facebook 	= get_post_meta( $post->ID, 'cf_facebook', true );
    ?>
    <table class="form-table">
        <tbody>
	        <tr class="form-field">
				<th scope="row">
					<label for="cf_position">Position</label> 
				</th>
  				<td>
  					<input class="code" name="cf_position" id="cf_position" type="text" value="<?php echo $cf_position; ?>" /> 
  				</td>
		    </tr>
		    <tr class="form-field">
				<th scope="row">
					<label for="cf_twitter">Twitter URL</label> 
				</th>
  				<td>
  					<input class="code" name="cf_twitter" id="cf_twitter" type="text" value="<?php echo $cf_twitter; ?>" /> 
  				</td>
		    </tr>
		    <tr class="form-field">
				<th scope="row">
					<label for="cf_facebook">Facebook URL</label> 
				</th>
  				<td>
  					<input class="code" name="cf_facebook" id="cf_facebook" type="text" value="<?php echo $cf_facebook; ?>" /> 
  				</td>
		    </tr>
	    </tbody>
	</table>

    <?php
}

/**
 * Team Member custom meta fields save
 */
function cf_member_details_callback_save_meta_box( $post_id ) 
{
    if ( ! isset( $_POST['cf_details_nonce'] ) )
        return $post_id;

    $nonce = $_POST['cf_details_nonce'];

    if ( ! wp_verify_nonce( $nonce, 'cf_details' ) )
        return $post_id;

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return $post_id;

    if ( 'cf_team_member' == $_POST['post_type'] ) 
    {
        if ( ! current_user_can( 'edit_page', $post_id ) )
            return $post_id;
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) )
            return $post_id;
    }

    $data['cf_position'] 	= sanitize_text_field( $_POST['cf_position'] );
    $data['cf_twitter'] 	= sanitize_text_field( $_POST['cf_twitter'] );
    $data['cf_facebook'] 	= sanitize_text_field( $_POST['cf_facebook'] );

    foreach ($data as $key => $value) 
    {
        if( $post->post_type == 'revision' ) return; 
        update_post_meta($post_id, $key, $value);
    }

}
add_action( 'save_post', 'cf_member_details_callback_save_meta_box' );

/**
 * Team Member shortcode
 */
add_shortcode( 'team-members', 'cf_team_member_grid' );
function cf_team_member_grid()
{
	wp_enqueue_script( 'cf_script', plugin_dir_url( __FILE__ ).'js/cf_script.js', array('jquery'), false, true );
	wp_enqueue_style( 'cf_grid', plugin_dir_url( __FILE__ ).'css/cf_grid.css', array(), false, 'all' );
	wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), false, 'all' );
	include 'grid-team-member.php';
}