jQuery.noConflict();

jQuery(document).ready(function(){
	jQuery(document).on('click', '.team-member .read-more', function(e){
		e.preventDefault();

		var tempHeight;
		var tempString;
		var sibling = jQuery(this).siblings('.additional-content');
		var elem = jQuery(this);	

		if (!sibling.is(':visible')){
			sibling.show();
			tempHeight = sibling.outerHeight();
			sibling.css('height', 0);

			sibling.animate({
				height: tempHeight
			}, 400, function() {
				tempString = elem.html();
				elem.text(elem.data('alt'));
				elem.data('alt', tempString);
			});
		} else {
			sibling.animate({
				height: 0
			}, 400, function() {
				tempString = elem.html();
				elem.text(elem.data('alt'));
				elem.data('alt', tempString);
				sibling.hide().css('height', 'auto');
			});
		}
	});
});