<?php
$args = array(
	'post_type'		=> 'cf_team_member',
	'post_status'	=> 'publish',
	'posts_per_page'=> 12,
	'order_by'		=> 'title',
	'order'			=> 'ASC'
);
$the_query = new WP_Query( $args );
?>

<div class="grid-container">
	<div class="grid-row">
		<?php
		if ( $the_query->have_posts() ) 
		{
			while ( $the_query->have_posts() ) 
			{
				$the_query->the_post();
				$image 			= get_the_post_thumbnail_url( $the_query->post->ID, 'medium' );
				$cf_position 	= get_post_meta( $the_query->post->ID, 'cf_position', true );
				$cf_twitter 	= get_post_meta( $the_query->post->ID, 'cf_twitter', true );
				$cf_facebook 	= get_post_meta( $the_query->post->ID, 'cf_facebook', true );
				?>

				<div class="grid-item">
					<div class="team-member">
						<div class="image-container" style="background-image: url(<?php echo $image; ?>);"></div><!-- /.image-container -->
						<div class="text-content">
							<h3 class="member-name"><?php the_title(); ?></h3><!-- /.member-name -->
							<h5 class="member-position"><?php echo esc_html($cf_position); ?></h5>

							<div class="member-socials">
								<a class="cf_facebook" href="<?php echo esc_html($cf_facebook); ?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
								<a class="cf_twitter" href="<?php echo esc_html($cf_twitter); ?>"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
							</div><!-- /.member-socials -->

							<div class="additional-content">
								<?php the_content(); ?>
							</div><!-- /.additional-content -->

							<a href="#" class="read-more" data-alt="Read Less">Read More</a>
						</div><!-- /.text-content -->
					</div><!-- /.team-member -->
				</div><!-- /.grid-item -->

				<?php
			}
			wp_reset_postdata();
		}
		?>
	</div><!-- /.grid-row -->
</div><!-- /.container -->